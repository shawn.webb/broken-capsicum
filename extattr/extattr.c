#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>

#include <sys/types.h>
#include <sys/capsicum.h>
#include <sys/extattr.h>
#include <libutil.h>

int
main(int argc, char *argv[])
{
	cap_rights_t rights;
	char buf[1024];
	int fd, ns;

	if (argc != 2) {
		printf("USAGE: %s /path/to/file\n", argv[0]);
		exit(1);
	}

	fd = open(argv[1], O_PATH | O_CLOEXEC);
	if (fd == -1) {
		perror("open");
		exit(1);
	}

	memset(&rights, 0, sizeof(rights));
	cap_rights_init(&rights, CAP_EXTATTR_GET);
	cap_rights_limit(fd, &rights);

	cap_enter();

	extattr_string_to_namespace("system", &ns);

	if (extattr_get_fd(fd, ns, "test-01", &buf, sizeof(buf)) == -1) {
		perror("extattr_get_fd");
		exit(1);
	}

	close(fd);
	return (0);
}
